<?php
include($_SERVER['DOCUMENT_ROOT'] . '/includes/connect_api.php');
include($_SERVER['DOCUMENT_ROOT'] . '/includes/data/api_resources.php');

// Base URL of Twitch api
$streamsApi = "https://api.twitch.tv/helix/streams";
// clientId for interacting with Twitch API. refreshable at https://dev.twitch.tv/dashboard
$clientId = "hn8w18i343n3hl3tx5y3sxk0sh24o2";

// Gets id that's identifiable by Twitch API for all streamers registered locally
$query = "SELECT twitch_id FROM twitch_channels";
$result = mysqli_query($con, $query);
if (!$result) die (mysqli_error($con));
$rows = mysqli_num_rows($result);
if ($rows == 0) echo "No streamers found.<br>";

$alltwitchids = array();

$statusDictionary = [
    "offline" => 0,
    "live" => 1,
    "vodcast" => 2];


// Translates the mysql associative arrays to a single list of all the streamers to refresh
while ($row = mysqli_fetch_assoc($result)) {
    $alltwitchids[] = $row['twitch_id'];
}

// Separates the streamers in batches of 100, this is because the Twitch API can only proces up to 100 lookups per API call
for ($i = 0; $i <= (ceil($rows/100)-1); $i++) {
    // Concatenates user_ids into a single API request
    $twitchids = "?";
    $twitchIdArray = array();
    for ($ii = 1; ($i*100+$ii) <= $rows; $ii++) {
        $twitchids .= "user_id=" . $alltwitchids[$i*100+$ii-1] . "&";
        $twitchIdArray[] = $alltwitchids[$i*100+$ii-1];
    }
    $twitchids = substr($twitchids, 0, -1);
    
    // Settings for curl API call
    $responsearray = api_call_simple($streamsApi, $twitchids);
    
    $insertOnlineValues = array();
    $insertOfflineValues = array();
    $processedIds = array();
    $gameIds = array();
    
    foreach ($responsearray['data'] as $stream) {
        $streamtitle = mysqli_real_escape_string($con, $stream['title']);
        if (!$stream['game_id']) {$game_id = 0;}
        else {$game_id = $stream['game_id'];}
        $insertString = "(" . $stream['user_id'] . ",'" . $statusDictionary[$stream['type']] . "'," . $game_id . ",'" . $streamtitle . "'," . $stream['viewer_count'] . ",'" . $stream['started_at'] . "','" . $stream['language'] . "','" . $stream['thumbnail_url'] . "')";
        $insertOnlineValues[] = $insertString;
        echo mb_detect_encoding($streamtitle);
        echo " - ";
        echo $stream['title'];
        echo "<br>";
        printf("Initial character set: %s ", $con->character_set_name());
        $processedIds[] = $stream['user_id'];
        
        
        /*$currentid = $stream['user_id'];
        echo $currentid;
        $inserttestquery = "INSERT INTO twitch_channels (twitch_id) VALUES ($currentid)
        ON DUPLICATE KEY UPDATE twitch_id=VALUES(twitch_id);";
        $inserttestresponse = mysqli_query($con, $inserttestquery);
        if (!$inserttestresponse) die (mysqli_error($con));*/
    }
    
    // Does something to offline streams
    $offlineIds = array_diff($twitchIdArray, $processedIds);
    foreach ($offlineIds as $offlineId) {
        $insertString = "(" . $offlineId . "," . 0 . "," . 0 . ")";
        $insertOfflineValues[] = $insertString;
    }
    
    if (count($processedIds)>0) {
        $insertOnlineQueryValues = implode(",",$insertOnlineValues);
        $insertOnlineQuery = "INSERT INTO twitch_stream (twitch_id,status_id,game_id,title,viewers,start,language,preview) VALUES $insertOnlineQueryValues
        ON DUPLICATE KEY UPDATE status_id=VALUES(status_id),game_id=VALUES(game_id),title=VALUES(title),viewers=VALUES(viewers),start=VALUES(start),language=VALUES(language),preview=VALUES(preview);";
        echo $insertOnlineQuery;
        $insertOnlineResult = mysqli_query($con, $insertOnlineQuery);
        if (!$insertOnlineResult) die (mysqli_error($con));
    }
    
    if (count($offlineIds)>0) {
        $insertOfflineQueryValues = implode(",",$insertOfflineValues);
        $insertOfflineQuery = "INSERT INTO twitch_stream (twitch_id,status_id,viewers) VALUES $insertOfflineQueryValues
        ON DUPLICATE KEY UPDATE status_id=VALUES(status_id),viewers=VALUES(viewers);";
        echo $insertOfflineQuery;
        $insertOfflineResult = mysqli_query($con, $insertOfflineQuery);
        if (!$insertOfflineResult) die (mysqli_error($con));
    }
}

/*RefreshGamesTable($con, $clientId);*/

function api_call($apiUrl, $arguments) {
    global $clientId;
    
    $request = curl_init();
    curl_setopt_array($request, array(
        CURLOPT_URL => $apiUrl . "?first=100",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        'Client-ID: ' . $clientId)
    ));
    $response = curl_exec($request);
    $err = curl_error($request);
    curl_close($request);
    $responsearray = json_decode($response, true);
    return $responsearray;
}
?>


<script>
// Refreshes page to execute PHP script every 5 minutes.
setTimeout(function () { window.location.reload(); }, 5*60*1000);
// Shows time of last refresh.
document.write(new Date());
</script>