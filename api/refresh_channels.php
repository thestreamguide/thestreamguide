<!-- Koden skrevet af Asger Møller -->
<?php
include($_SERVER['DOCUMENT_ROOT'] . '/includes/connect_login.php');
include($_SERVER['DOCUMENT_ROOT'] . '/includes/data/api_resources.php');

// Base URL of Twitch api. Information about the object returned by the API call can be found at https://dev.twitch.tv/docs/api/reference#get-streams
$usersApi = "https://api.twitch.tv/helix/users";
// clientId for interacting with Twitch API. Client ids are managed at https://dev.twitch.tv/dashboard - Note that this ID is different from what's used in the authentication functionality to reduce request throttling
$clientId = "hn8w18i343n3hl3tx5y3sxk0sh24o2";

$timeStart = microtime(true);

// Gets id that's identifiable by Twitch API for all streamers registered locally
$query = "SELECT twitch_id FROM twitch_channel";
$result = mysqli_query($con, $query);
if (!$result) die (mysqli_error($con));
$rows = mysqli_num_rows($result);
if ($rows == 0) echo "No streamers found.<br>";

$allTwitchIds = array();

// The dictionary must mirror the statuses table of the database
$broadcasterDictionary = [
    "" => 0,
    "affiliate" => 1,
    "partner" => 2];

$apiToDatabaseDictionary = [
    "id" => "twitch_id",
    "login" => "login_name",
    "display_name" => "display_name",
    "description" => "description",
    "profile_image_url" => "profile_image",
    "offline_image_url" => "offline_image",
    "view_count" => "view_count",
    "broadcaster_type" => "broadcaster_type"];

// Translates the mysql associative arrays to a single list of all the streamers that are to be refreshed
while ($row = mysqli_fetch_assoc($result)) {
    $allTwitchIds[] = intval($row['twitch_id']);
}

// Separates the streamers in batches of 100, this is because the Twitch.tv API can only process up to 100 lookups per API call
for ($i = 0; $i <= (ceil($rows/100)-1); $i++) {
    // Concatenates user_ids into a single string for the API call
    $twitchIds = "id=";
    $twitchIdArray = array();
    for ($ii = 1; (($i*100)+$ii) <= $rows && (($i*100)+$ii) <= (($i+1)*100); $ii++) {
        $twitchIdArray[] = $allTwitchIds[($i*100)+($ii-1)];
    }
    $twitchIds = $twitchIds . implode("&id=",$twitchIdArray);
    $responseArray = api_call_simple($usersApi, $twitchIds);
    $insertValues = array();
    
    // Parse information about each streamer from the decoded JSON object returned by the api_call function and prepare the values for insertion in the mySQL query
    foreach ($responseArray['data'] as $user) {
        if ($user['login']) {
            $insertString = "(";
            foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
                $rowValue = $user[$apiColumn];
                if ($apiColumn == 'broadcaster_type') {$insertString .= $broadcasterDictionary[$rowValue];}
                else if (is_string($rowValue)){
                    $insertString .= "'";
                    if ($apiColumn === 'description' || $apiColumn === 'login' || $apiColumn === 'display_name') {
                    $insertString .= mysqli_real_escape_string($con, $rowValue);
                    } else {$insertString .= $rowValue;}
                    $insertString .= "'";
                } else {$insertString .= $rowValue;}
                $insertString .= ",";
            }
            $insertString = mb_substr($insertString, 0, -1);
            $insertString .= ")";
            $insertValues[] = $insertString;
        }
    }
    
    
    // Insert information about users in the twitch_channel table
    $insertQueryValues = implode(",",$insertValues);
    $insertQuery = "INSERT INTO twitch_channel (";
    $insertDuplicates = "";
    foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
        $insertQuery .= $databaseColumn . ",";
        $insertDuplicates .= $databaseColumn . "=VALUES(" . $databaseColumn . "),";
    }
    $insertQuery = mb_substr($insertQuery, 0, -1);
    $insertDuplicates = mb_substr($insertDuplicates, 0, -1);
    $insertQuery .= ") VALUES $insertQueryValues
    ON DUPLICATE KEY UPDATE $insertDuplicates;";
    $insertResult = mysqli_query($con, $insertQuery);
    if (!$insertResult) die (mysqli_error($con));
}

refresh_games_table($con, $clientId);

$timeEnd = microtime(true);
$executionTime = ($timeEnd - $timeStart);
echo '<br><b>Total Execution Time:</b> '.$executionTime.' seconds.<br>';
?>


<script>
// Refreshes page to execute PHP script every hour. Optimally this script is run as a serverside cron task
setTimeout(function () { window.location.reload(); }, 60*60*1000);
// Shows time of last refresh.
document.write(new Date());
</script>