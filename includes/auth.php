<!-- Koden skrevet af Klaus Gregersen -->
<?php

// If a user is not currently logged in, log the user in via their cookie if one is set, else do nothing
if(!isset($_SESSION['userId'])) {
    if(isset($_COOKIE['login_token'])) {
        // Separate the selector token and validator token from the users the users login cookie
        list($selector, $authenticator) = explode(':', $_COOKIE['login_token']);
        // Query the database to retrieve the stored hashed user validator token for comparison
        $cookieAuthQuery = "SELECT * FROM cookie_auth_tokens WHERE cookie_auth_selector = '$selector'";
        $cookieAuthResult = mysqli_query($con, $cookieAuthQuery);
            if (!$cookieAuthResult) die (mysqli_error($con));
        if ($cookieAuthResult) {
            $authData = mysqli_fetch_assoc($cookieAuthResult);
            // Compare the hashes of the users validator token stored in their cookie with the hashed version from the database
            if (hash_equals($authData['cookie_auth_token'], hash('sha256', base64_decode($authenticator)))) {
                // Retrieve user data if hashes match
                $userId = $authData['user_id'];
                $userInfoQuery = "SELECT users.role_id, users.twitch_id, twitch_channel.display_name, twitch_channel.profile_image
                FROM users
                INNER JOIN twitch_channel ON users.twitch_id=twitch_channel.twitch_id
                WHERE users.user_id = $userId;";
                $userInfoResult = mysqli_query($con, $userInfoQuery);
                    if (!$userInfoResult) die (mysqli_error($con));
                $userInfo = mysqli_fetch_assoc($userInfoResult);
                // Set user data in session for usage in functions related to the users identity
                $_SESSION['userId'] = $userId;
                $_SESSION['twitchId'] = $userInfo['twitch_id'];
                $_SESSION['displayName'] = $userInfo['display_name'];
                $_SESSION['roleId'] = $userInfo['role_id'];
                if ($userInfo['profile_image']) {
                    $_SESSION['profileImage'] = $userInfo['profile_image'];
                } else {
                    $_SESSION['profileImage'] = NULL;
                }
            } else {
                // If hash comparison fails to match, delete the existing cookie by setting the expiry date in the past
                echo "Authentification failed - bad Cookie!";
                $cookie_name = "login_token";
                $cookie_value = "Deleting cookie";
                $cookie_expire = time() - 1000;
                $cookie_path = "/";
                $cookie_domain = "";
                $cookie_secure = FALSE;
                $cookie_httpOnly = TRUE;
                
                setcookie($cookie_name, $cookie_value, $cookie_expire, $cookie_path, $cookie_domain, $cookie_secure, $cookie_httpOnly);
            }
        }
    }
}
?>