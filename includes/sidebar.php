<!-- Koden skrevet af: Asger Smedegaard -->
<div class="sideBar" id="sidebar">
    <p id="mobilehint">Show filters</p>
    <script>
        $(document).ready(function() {
            // add scroll to sidebar wrappers with the SlimScroll library
            $('#tagWrapper').slimScroll({
                position: 'right',
                height: 'auto',
            });
            $('#gamesWrapper').slimScroll({
                position: 'right',
                height: 'auto',
            });
            
            if ($(".tags")) {
                 $(".chosenFilter").nextAll("input").attr('name',''); 
            }
            $(".sidebargame:not('.chosenFilter')").attr('name','game');
            var test = $(".chosenFilter.sidebargame").attr('id');
            $('#'+test).attr('name','');
            console.log('This tag is chosen: ' + test); 

            //functionality of the sidebar game search 
            $('#gameSearch').keyup(function(e) {      
                // create the regular expression
                var regEx = new RegExp($.map($(this).val().trim().split(' '), function(v) {
                    return '(?=.*?' + v + ')';
                }).join(''), 'i');

                // select all list items, hide and filter by the regex then show
                $('.sidebargame').hide().filter(function() {
                    return regEx.exec($(this).val());
                }).show();
            }); 
                //functionality of the sidebar tag search    
            $('#tagSearch').keyup(function(e) {      
                // create the regular expression
                var regEx = new RegExp($.map($(this).val().trim().split(' '), function(v) {
                    return '(?=.*?' + v + ')';
                }).join(''), 'i');

                // select all list items, hide and filter by the regex then show
                $('.tagContainer').hide().filter(function() {
                   return regEx.exec($(this).data('tag'));
                }).show();
            });
        });
    </script>
  
    <!-- Icons from Font Awesome free for commercial use -->
    <h3>Start Filtering Here!</h3>
     <hr>
      <div id="reset_button_container">
           <span>Reset filtering:</span>
            <form method="GET">
                <!-- standard form bruger metoden get som ligger sig i url -->
                <input class="sidebaritem" id="reset_button" title="Reset filters" type="submit" name="resetButton" value="" />
            </form>
        </div>
            
    <div id="populargames">
        <div class="searchWrapper">
            <img class="h2Img" alt="Games" src="/img/font-awesome_4-7-0_gamepad_48.png">
            <input class="sidebarSearch" id="gameSearch" type="text" placeholder="Find a Game..." >
            
        </div>
        <div id="gamesSidebar" class="gamessidebar">
            <div id="gamesWrapper">
                <form id="gamesform" method='GET' action=' <?php echo "$baseUrl/discover.php"?>'>
                    <?php 
                    if (isset($_GET['tag'])){
                        $tagvalue = $_GET['tag'];
                        echo '<input type="hidden" name="tag" value="'.$tagvalue.'"/>'; 
                    };
                    dbGetGamesSidebar($con, TRUE);
                    ?>
                </form>
            </div>
            <div class=sidebarShadow></div>
        </div>
    </div>
        
    <div class="searchWrapper">
        <img class="h2Img" alt="Tags" src="/img/font-awesome_4-7-0_hashtag_48.png">
        <input class="sidebarSearch" id="tagSearch" type="text" placeholder="Find a Tag...">
    </div>
    <div id="popTags">
        <div id="tagWrapper">

        <?php
        include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/dbgrab_channel.php');
        $sql = "SELECT tag, tag_icon FROM rates ORDER BY rating DESC LIMIT 9"; //opretter en variabel som stemmer overens med det som skal stå i query funktionen

        $result = $con->query($sql); //opretter varialben result og sætter den til at være = conn hvor vi henter funktionen inde i connection som hedder query og indsætter sql dataene i denne funktion som henter dataene fra sql databasen.

        while($row = $result->fetch_assoc()) //mens row = associate array print beskeden under. dvs den går fra row 1 til row 2 til row 3 osv indtil der kommer et punkt i tabellen hvor der ikke står data da den så er row = null
            
        //beskeden som printes er html php code som printes til html kode som er en form der benyttes til at gemme tags (og display et icon af taggen) samt spillet hvis dette er valgt ved hjælp af isset.
        {
            echo "<form class='tagForm' method='GET' action='" . $baseUrl . "/discover.php'>";
            echo "<div title='".$row['tag']."' data-tag=".$row['tag']." class='tagContainer'>";
            if (isset($_GET['game'])) {
                $tagvalue = $_GET['game'];
                echo '<input type="hidden" name="game" value="'.$tagvalue.'"/>';
            }
            ?>
            <!-- beatify somehow destroys this -->
      <div class="tagTextWrapper" data-size="<?php echo $row['tag'] ?>">
            <input id="<?php echo htmlspecialchars($row['tag']) ?>" class='sidebaritem tags' type='submit' value="" style='background-image: url( <?php echo $row['tag_icon']; ?> )'/>
            <p><?php echo $row['tag'] ?></p>
             <input type="hidden" name="tag" value="<?php echo $row['tag'] ?>" />
   </div>
            <?php
            echo "</div>";
            echo "</form>";
        }
        ?>
            <!-- The graphics of chill.png and amazing.png are credited to Zee Que of http://www.designbolts.com/ -->
        </div>
        <div class=sidebarShadow></div>
    </div>
</div>

<?php
// Inserts JavaScript to add the class chosenFilter to the chosen tags and games for styling in CSS with a special selector
if (isset($_GET['tag'])) {
    $chosenTag = clean($_GET['tag']);
    echo "<script>";
    echo "document.getElementById('$chosenTag').classList.add('chosenFilter');";
    echo "</script>";
}

if (isset($_GET['game'])) {
    $chosenGame = clean($_GET['game']);
    echo "<script>";
    echo "document.getElementById('$chosenGame').classList.add('chosenFilter');";
    echo "</script>";
}

function dbGetGamesSidebar($con, $order = NULL) {
    // This function is a slight variation of dbGetGames from dbgrab_discover.php fitted for the purpose of the Sidebar. The main variations are the number of games and the dimensions of the pictures.
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('90','117');
    
    if ($order != NULL){
        $orderText = "ORDER BY games.game_viewers DESC";
    } else {
        $orderText = NULL;
    }
    
    $query = "SELECT * FROM games WHERE games.game_viewers > 0 $orderText";
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_array($result)){
        $gameName = $row['game_name'];
        $gameArt = $row['game_art_url'];
        $gameId = clean($row['game_name']);
        
    ?>

    <input id="<?php echo htmlspecialchars($gameId); ?>" class="sidebaritem sidebargame" type="submit" name="game" value="<?php echo $gameName ?>"  style="background-image:url('<?php echo (str_replace($sS, $sR, $gameArt)) ?>')" title="<?php echo $gameName ?>">
    <?php
    };
}

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

?>
