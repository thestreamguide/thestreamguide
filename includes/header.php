<!-- Koden skrevet af: Mikkel Kokholm -->
<?php
$baseUrl = "//localhost"; // "//thestream.guide" for live ellers //localhost
// Starts a session if none is set.
if(!isset($_SESSION)) {
    session_start();
}
// Include scripts for a mysql connection and necessary functions and variables to interact with the Twitch API
include ($_SERVER['DOCUMENT_ROOT'] . '/includes/connect_login.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/api_resources.php');
// Runs the auth document to check a users login status and log users with a persistent log in cookie set into the site.
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/auth.php');
// Dynamically set page title based on the name of the open document
$page = basename($_SERVER['PHP_SELF']);
switch($page)
{
    case 'index.php':
        $title = 'Welcome to thestream.guide';
        break;
    case 'channel.php':
        $title = 'Channel page';
        break;
    case 'discover.php':
        $title = 'Discover Streamers';
        break;
    case 'profile.php':
        $title = 'Profile and stream management';
        break;
    case 'connected.php':
        $title = 'Connected to Twitch.tv!';
        break;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title; ?></title>
    <meta charset="UTF-8">
    <meta name="Description" content="Welcome to TheStream.Guide">
    <meta name="keywords" content="Streaming Games">
    <meta name="author" content="Gruppe 2 LOOP 3">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/styles/davis.css">
    <link rel="stylesheet" type="text/css" href="/styles/normalize.css">
    <link rel="stylesheet" type="text/css" href="styles/navcss.css?">
    <script src="https://code.jquery.com/jquery-latest.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="scripts/jquery.slimscroll.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
</head>
<body>
    <header>
        <!-- Navigation bar start -->
        <div id="topNav" class="nav">
            <div id="leftNav" class="navContainer">
                <a id="navHome" href="<?php echo $baseUrl ?>">Thestream.guide</a>
                <a <?php if ($page == "discover.php") echo "class=active" ?> href="<?php echo $baseUrl ?>/discover.php">Discover</a>
                <a <?php if ($page == "channel.php") echo "class=active" ?> href="<?php echo $baseUrl ?>/channel.php">Channel</a>
                <!-- ikoner er hentet fra denne side hvor de er gratis https://www.iconfinder.com/-->
            </div>
            <div id="rightNav" class="navContainer">
                <a class="navWithPic<?php if ($page == "profile.php") echo " active" ?>" href=
                <?php if (isset($_SESSION['userId'])) {
                    // Sets either a profile icon and profile name or a login button depending on the users login status in the Session variable
                    $profileImage = $_SESSION['profileImage'];
                    if (is_null($profileImage)) {
                        $profileImage = ($baseUrl . "/img/standardprofileimage.jpg");
                    }
                    $displayName = $_SESSION['displayName'];
                    echo '"'.$baseUrl.'/profile.php">Hi '.$displayName.'!<img class="navprofileimage navPic" src="'.$profileImage.'" alt="User Profile Image"/></a>';
                } else {
                    echo '"'.$baseUrl.'/loginwithtwitch.php">Connect with<img class="navPic" src="img/Glitch_Black_RGB.png" alt="Twitch"/></a>';
                } ?>
            <a href="javascript:void(0);" id="mobileBurger" onclick="burgerMenu()">&#9776;</a>
            </div>
            <div id="mobileNav" class="mobileNav">
                <a <?php if ($page == "discover.php") echo "class=active" ?> href="<?php echo $baseUrl ?>/discover.php">Discover</a>
                <a <?php if ($page == "channel.php") echo "class=active" ?> href="<?php echo $baseUrl ?>/channel.php">Channel</a>
                <a class="navWithPic<?php if ($page == "profile.php") echo " active" ?>" href=<?php
                if (isset($_SESSION['userId'])) {
                    echo '"'.$baseUrl.'/profile.php">Hi '.$displayName.'!<img class="navprofileimage navPic" src="'.$profileImage.'" alt="User Profile Image"/></a>';
                } else {
                    echo '"'.$baseUrl.'/loginwithtwitch.php">Connect with<img class="navPic" src="img/Glitch_Black_RGB.png" alt="Twitch"/></a>';
                } ?>
            </div>
        </div>
    <script>
        function burgerMenu() {
            var x = document.getElementById("mobileNav");
            if (x.className === "mobileNav") {
                x.className += " shown";
            } else {
                x.className = "mobileNav";
            }
        }
        if( $('#mobileBurger').css('display')!='none') {
            var didScroll;
            var lastScrollTop = 0;
            var delta = 5;
            var navbarHeight = $('header').outerHeight();

            $(window).scroll(function(event){
                didScroll = true;
            });

            setInterval(function() {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 250);

            function hasScrolled() {
                var st = $(this).scrollTop();

                // Make sure they scroll more than delta
                if(Math.abs(lastScrollTop - st) <= delta)
                    return;

                // If they scrolled down and are past the navbar, add class .nav-up.
                // This is necessary so you never see what is "behind" the navbar.
                if (st > lastScrollTop && st > navbarHeight){
                    // Scroll Down
                    $('header').removeClass('nav-down').addClass('nav-up');
                    $('#sidebar').removeClass('nav-down').addClass('nav-up');
                    $('#mobileNav').removeClass('nav-down').addClass('nav-up');
                } else {
                    // Scroll Up
                    if(st + $(window).height() < $(document).height()) {
                        $('header').removeClass('nav-up').addClass('nav-down');
                        $('#sidebar').removeClass('nav-up').addClass('nav-down');
                        $('#mobileNav').removeClass('nav-up').addClass('nav-down');
                    }
                }

                lastScrollTop = st;
            }
        }
    </script>
</header>