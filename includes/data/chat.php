<!-- Koden skrevet af Christian Davis -->
<?php
//Dette embedder chat'en til en give twitch kanal. $channel er den samme som parameteren der embedder en stream
echo <<< EOT
        
 <!--Embed twitch Chat -->
<iframe frameborder="0" 
        scrolling="no" 
        id="chat_embed" 
        src="http://www.twitch.tv/$channel/chat" 
        height="auto" 
        width=275px>
</iframe>

EOT;
?>