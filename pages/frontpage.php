<!-- Koden skrevet af Christian Davis & Asger Smedegaard & Klaus Gregersen -->
<main>
<?php 
    include($_SERVER['DOCUMENT_ROOT'] . "/includes/data/dbgrabfrontpage.php");
?>
<div id="randomStream">
    <?php
            $randTwitchId = getRandomUser($con);
            //Gets a random streamer name based on the random twitch id
            $channel = dbFetch($con,$randTwitchId, 'login_name');
            
            //includes the stream
            include($_SERVER['DOCUMENT_ROOT'] . "/includes/streamfrontpage.php");
            
            //Henter alt data'en fra db'en
    ?>
        <div id="contentStream">

            <div id="title">

                <div id="titleContent">
                    <div class="titleCornor">
                    </div>
                    <?php e(dbFetch($con,$randTwitchId, 'title')); ?>
                </div>
            </div>

            <div id="info">
                <div class="infoCornor">
                </div>
                <div id="infoContent">
                    <strong><?php  e(dbFetch($con,$randTwitchId, 'display_name')); ?> </strong> is currently playing
                    <strong> <?php e(dbFetch($con,$randTwitchId, 'game_name')); ?>  </strong> and has
                    <strong> <?php e(dbFetch($con,$randTwitchId, 'viewers')); ?> </strong> viewers
                </div>
                <div id="tags">
                    <?php dbSetupStreamerGetTags($con, $randTwitchId); ?>
                </div>
            </div>

        </div>
</div>

<div id="recommendedStreamers">
    <h1>Our Recommended Streamers:</h1> <br>
    <div id="streamers">
        <?php dbSetupStreamerFrontpage($con, NULL, NULL, 6); ?>
    </div>
</div>

<div id="gameSection">
    <h1>Choose from games below:</h1> <br>
    <div id="games">

        <form method='GET' action='discover.php'>
            <?php dbGetGamesFrontpage($con, 1);?>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".cdStreamerDiv").click(function() {
            $(this).find(".inputFormHidden").submit();
        });
    });
</script>
</main>
<?php
function e($t = NULL){
    echo $t;
}
?>