<!-- Koden skrevet af Mikkel Kokholm -->
<main>
<div id="profilePage">
<?php
// Include the require_login script to forbid permission to users that aren't logged in
// require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/data/require_login.php");

// Includes the logout script if the user has pressed the logout button
if (isset($_POST['logout'])) {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/data/logout.php");
}

// Look up info for the particular user registered in the Session for later use
$streamerInfoQuery = "SELECT * FROM streamer_info
    WHERE user_id = " . $_SESSION['userId'] . ";";
$streamerInfoResult = mysqli_query($con, $streamerInfoQuery);
$streamerInfo = mysqli_fetch_assoc($streamerInfoResult);

// Check which content to print on the page based on the users roleId. roleId 2 is a streamer and 1 is a regular viewer
if ($_SESSION['roleId'] != 2) {
    if (isset($_POST['streamer'])) {
        $registerStreamerQuery = "UPDATE users
            SET role_id = 2
            WHERE user_id =" . $_SESSION['userId'] . ";";
        $registerStreamerResult = mysqli_query($con, $registerStreamerQuery);
        $_SESSION['roleId'] = 2;
        if (isset($registerStreamerResult) && is_resource($registerStreamerResult)) {
            mysqli_free_result($registerStreamerResult);
        } elseif (!$registerStreamerResult) {
            echo "Registration failed: " . mysqli_error($con) . "<br>";
        }
        print_input_forms($streamerInfo);
    } else {
    // Print a button for a user to register as a streamer
    echo "<form method='post'>";
    echo "<input id='doYouStream' type='submit' name='streamer' value='Do you stream?'>";
    echo "</form>";
    }
} else {
    // Print forms for the streamer to update his/her information
    print_input_forms($streamerInfo);
}

// Updates user information if any input has been given in the streamer info forms
if (isset ($_POST['facebook_url']) || isset($_POST['twitter_url']) || isset($_POST['youtube_url']) || isset($_POST['description'])) {
    // Define variables for insertion utilizing get_post function with built in mysql string escaping
    if (isset ($_POST['facebook_url'])) {
        $facebook_url = get_post($con, 'facebook_url');
    } else {
        $facebook_url = NULL;
    }
    if (isset ($_POST['twitter_url'])) {
        $twitter_url = get_post($con, 'twitter_url');
    } else {
        $twitter_url = NULL;
    }
    if (isset ($_POST['youtube_url'])) {
        $youtube_url = get_post($con, 'youtube_url');
    } else {
        $youtube_url = NULL;
    }
    if (isset ($_POST['description'])) {
        $description = get_post($con, 'description');
    } else {
        $description = NULL;
    }
    // Insert the users information if no prior info is found. Otherwise update the information
    $query = "INSERT INTO streamer_info (user_id,
        facebook_url,
        twitter_url,
        youtube_url,
        description)
        VALUES (" . $_SESSION['userId'] . ",
        '$facebook_url',
        '$twitter_url',
        '$youtube_url',
        '$description')
        ON DUPLICATE KEY UPDATE facebook_url=VALUES(facebook_url),
        twitter_url=VALUES(twitter_url),
        youtube_url=VALUES(youtube_url),
        description=VALUES(description);";
    $result = mysqli_query($con, $query);
    if ($result) echo "<p class='serverResponse'>Updated your information.</p>";
    if (isset($result) && is_resource($result)){
        mysqli_free_result($result);
    } elseif (!$result) {
        echo "<p class='serverResponse'>Insert failed: " . mysqli_error($con) . "</p>";
    }
    header("Location: $baseUrl/profile.php");
}

// Updates streamer tag information if a tag has been selected.
if (isset($_POST['tag'])) {
    // Validate user identity with Twitch API utilizing a function from api_resources
    try {
        $validuser = validate_user($_SESSION['userId'], $con);
    } catch (Exception $e) {
        echo "<p class='serverResponse'>" . $e->getMessage() . "</p>";
    }
    if ($validuser == TRUE) {
        $tagId = get_post($con, 'tag_id');
        $twitchId = $_SESSION['twitchId'];
        $checkStreamerTagsQuery = "SELECT * FROM get_streamer_tags
            WHERE twitch_id = $twitchId AND
            tag_id = $tagId;";
        $checkStreamerTagsResult = mysqli_query($con, $checkStreamerTagsQuery);
        $rows = mysqli_num_rows($checkStreamerTagsResult);
        if ($rows == 0) {
            $queryt = "INSERT INTO get_streamer_tags (twitch_id,
                tag_id,
                tag_active)VALUES ($twitchId,
                $tagId,
                1);";
            $result = mysqli_query($con, $queryt);
            if (isset($result) && is_resource($result)){
                mysqli_free_result($result);
            }
        } else {
            $streamerTag = mysqli_fetch_assoc($checkStreamerTagsResult);
            if ($streamerTag['tag_active'] == 0) {
                $queryt = "UPDATE get_streamer_tags 
                    set tag_active = 1
                    WHERE twitch_id = $twitchId AND
                    tag_id = $tagId;";
                $result = mysqli_query($con, $queryt);
                echo "<p class='serverResponse'>" . $_POST['tag'] . " is now active!</p>";
                if (isset($result) && is_resource($result)){
                    mysqli_free_result($result);
                }
            } else {
                $queryt = "UPDATE get_streamer_tags 
                    set tag_active = 0
                    WHERE twitch_id = $twitchId AND
                    tag_id = $tagId;";
                $result = mysqli_query($con, $queryt);
                echo "<p class='serverResponse'>" . $_POST['tag'] . " is no longer active.</p>";
                if (isset($result) && is_resource($result)){
                    mysqli_free_result($result);
                }
            }
        }
        if (isset($result) && is_resource($result)) {
            mysqli_free_result($checkStreamerTagsResult);
        }
    } else {
        echo "<p class='serverResponse'>Failed to validate user with Twitch.</p>";
    }
}

// Print the logout button
echo "<form method='post'>";
echo "<input id='logoutButton' type='submit' name='logout' value='Log Out'></form>";
// Print all input forms when called, text input fields are prefilled with current user information from the initial database query in this script
function print_input_forms($streamerInfo) {
    global $con;
    global $baseUrl;
    
    // Dynamic styling for filled and verified forms to let the user know their information is saved
    $fieldFilled = "border-right: 11px solid rgb(0,130,75)";
    $fieldUnfilled = "border-right: 7px solid rgb(130,75,0)";
    if ($streamerInfo['description'] == TRUE) {
        $descriptionStatus = $fieldFilled;
    } else {
        $descriptionStatus = $fieldUnfilled;
    }
    if ($streamerInfo['twitter_url'] == TRUE) {
        $twitterStatus = $fieldFilled;
    } else {
        $twitterStatus = $fieldUnfilled;
    }
    if ($streamerInfo['facebook_url'] == TRUE) {
        $facebookStatus = $fieldFilled;
    } else {
        $facebookStatus = $fieldUnfilled;
    }
    if ($streamerInfo['youtube_url'] == TRUE) {
        $youtubeStatus = $fieldFilled;
    } else {
        $youtubeStatus = $fieldUnfilled;
    }
    
    // Printing of forms and inputs
    echo "<h2 id='welcome'>Welcome ". $_SESSION['displayName'] . "</h2>";
    echo "<div id='profilePageWrapper'>";
    echo "<div id='streamerInfoDiv'>";
    echo "<h2>Please fill in your info below:</h2>";
    echo "<form id='streamerInfo' method='post'>";
    echo "Description: <input type='text' style='$descriptionStatus' name='description' placeholder='Enter a description' value='" . htmlspecialchars($streamerInfo['description'], ENT_QUOTES) . "' size='50'><br>";
    echo "<label>Twitter url:</label>";
    echo "<input type='url' style='$twitterStatus' name='twitter_url' placeholder='https://twitter.com/you' value='" . $streamerInfo['twitter_url'] . "' size='50'><br>";
    echo "Facebook url: <input type='url' style='$facebookStatus' name='facebook_url' placeholder='https://www.facebook.com/you' value='" . $streamerInfo['facebook_url'] . "' size='50'><br>";
    echo "Youtube url: <input type='url' style='$youtubeStatus' name='youtube_url' placeholder='https://www.youtube.com/user/you' value='" . $streamerInfo['youtube_url'] . "' size='50'><br>";
    echo "<input type='submit' value='Update Info'></form>";
    echo "</div>";
    echo "<div id='tagSelection'>";
    echo "<h2>What's your mood today?</h2>";
    echo "<div class='profileTagContainer'>";
    $sql = "SELECT tag_id, tag, tag_icon FROM rates ORDER BY rating DESC LIMIT 9"; //opretter en variabel som stemmer overens med det som skal stå i query funktionen
    $result = $con->query($sql); //opretter variablen result og sætter den til at være = conn hvor vi henter funktionen inde i connection som hedder query og indsætter sql dataene i denne funktion som henter dataene fra sql databasen.
    
    while($row = $result->fetch_assoc()) //mens row = associate array print beskeden under. dvs den går fra row 1 til row 2 til row 3 osv indtil der kommer et punkt i tabellen hvor der ikke står data da den så er row = null
    {
        echo "<div class='tagOptionContainer'>";
        echo "<form method='POST'>";
        echo "<input type='hidden' value='" . $row['tag_id'] . "' name='tag_id'/>";
        echo "<input class='tags' type='submit' value='" . $row['tag'] . "' name='tag' style='background-image: url(".$row['tag_icon'].")'/>";
        echo "</form>"; ?>
        <span> <?php echo $row["tag"] ?></span> <?php
        echo "</div>";
    }
    echo "</div>";
    echo "</div>";
    echo "</div>";
}

function get_post($con, $svar) {
    return mysqli_real_escape_string($con, $_POST[$svar]);
}
?>
</div>
</main>