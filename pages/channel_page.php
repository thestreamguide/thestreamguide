<!-- Koden skrevet af Christian Davis -->
<?php
include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/dbgrab_discover.php');
/* the following Code is written by Christian Davis */
 if (isset($_GET['channel'])) {
    $channel = $_GET['channel'];
} else if (!isset($_GET['channel'])) {
    $randTwitchId = getRandomUserCh($con);
    //Gets a random streamer name based on the random twitch id
    $channel = dbFetchCh($con,$randTwitchId, 'login_name');
} else {
    echo 'Something is very wrong regarding the $_GET';
}


if ($channel != NULL){
$channelName = dbSetupStreamerChannel($con,$channel,'display_name');
$id = dbSetupStreamerChannel($con,$channel,'twitch_id');
} else {
    $channelName = 'No channel was found: Error '.$channel.' = NULL';
}

//setting useful variables below v
$id; //is set to dbSetupStreamerChannel($con,$channel,'twitch_id');
$chId = $channel; //is set to dbFetchCh($con,$randTwitchId, 'login_name');
$chName = dbFetchCh($con, $id, 'display_name'); //get the displayed name from twitch
$game = dbFetchCh($con, $id, 'game_name'); //gets the name of the game streamer is playing
$gameArt = dbFetchCh($con, $id, 'game_art_url'); //gets the art the streamer if playing
$title = dbFetchCh($con, $id, 'title'); //gets the title the streamer has set 
$views = dbFetchCh($con,$id, 'viewers'); //gets the number of current viewers
?>
<main>
    <div id="channelPage">
        <div id="channelContent">
            <?php include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/stream.php'); ?>

        </div>

        <div id="streamInfo">
            <h3>
                <?php e($title) ?>
            </h3>
            <div id="container">
                <button id="toggleChat">Toggle Chat</button>
                <form method="GET" action="discover.php">
                    <div class="gamesWrapper">
                    <input id="imgGame" type="submit" name="game" value="<?php e($game) ?>" style="background-image:url('<?php e($gameArt) ?>')" title="<?php e($game) ?>" alt="<?php e($chName) ?> is currently playing <?php e($game) ?>" />
                    </div>
                </form>
                <div class="wrapper">
                <span> <strong><?php e($chName) ?> </strong> is currently playing <strong> <?php e($game) ?></strong> with <strong><?php e($views) ?></strong> current viewers.</span>
                <div class="infoWrapper">
                    
                    
                <div class="info">
                    <div class="topLeftCornor">
                    </div>
                    <div class="topRightCornor">
                    </div>
                    <div class="botLeftCornor">
                    </div>
                    <div class="botRightCornor">
                    </div>
                    <div class="wrap">
                        <div class="descTagWrap">
                             <p id="streamerDescription">Desc goes here</p>
                        </div>
                    </div>
                </div>
                <div class="info">
                    <div class="topLeftCornor">
                    </div>
                    <div class="topRightCornor">
                    </div>
                    <div class="botLeftCornor">
                    </div>

                    <div class="botRightCornor">
                    </div>
                    <div class="wrap">
                        
                        <div class="descTagWrap">
                           
                            <?php dbSetupStreamerGetTagsChannel($con, $id); ?>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
            <!-- Mangler so far:description, måske sociale medier -->
        </div>
    </div>
</main>
<?php include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/chat.php'); ?>

<script>
if( $('#mobileBurger').css('display')!='none') {
   $('#chat_embed').appendTo('#channelPage');
};
$(document).ready(function() {
    $("#toggleChat").click(function() {
        $("#chat_embed").toggle("fast")
    })
});
</script>

