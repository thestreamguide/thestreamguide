<?php
// Buffer all output from the script to be able to set a new header halfways in the script.
ob_start();
include($_SERVER['DOCUMENT_ROOT'] . "/includes/header.php");
include($_SERVER['DOCUMENT_ROOT'] . "/pages/ret_page.php");
include($_SERVER['DOCUMENT_ROOT'] . "/includes/footer.php");
ob_end_flush();
exit();
?>